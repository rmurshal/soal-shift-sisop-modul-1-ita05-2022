#2a
mkdir -p forensic_log_website_daffainfo_log

#2b
awk -F : '
  {
    if(NR!=1) {
      ++n
    }
  } END {
    print "Rata-rata serangan adalah sebanyak " (n)/12 " requests per jam"
  }
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt

#2c
awk -F : '
  {
    if(NR!=1) {
      print $1
    }
  }
' log_website_daffainfo.log | sort | uniq -c | sort -nr | head -n 1 | awk '
  {
    print "IP yang paling banyak mengakses server adalah: " $2 " sebanyak " $1 " requests \n"
  }
' >> forensic_log_website_daffainfo_log/result.txt

#2d
awk -F : '
/curl/ { ++n }
END {
  print "Ada " n " request yang menggunakan curl sebagai user-agent \n"
}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

#2e
awk -F : '
$3 == 02 { print $1 " jam 2 pagi" }
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
