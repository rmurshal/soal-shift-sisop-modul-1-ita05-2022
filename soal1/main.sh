#!/bin/bash

mkdir -p users
echo -n "" >> users/user.txt

login()
{
    read -p "Enter username : " username
    read -s -p "Enter password : " password

    datetime=$(date '+%m/%d/%y %H:%M:%S')

    #check user validation
    if grep -q "$username, $password" users/user.txt
    then
        echo -e "$datetime LOGIN:INFO User $username logged in!\n"
        echo "$datetime LOGIN:INFO User $username logged in!" >> log.txt

        #getting command from user  
        read -p "Enter command : " command
        case "$command" in
            "dl")

                read -p "how many pic(s) : " pics
                folder=$(date +%Y-%m-%d)_$username

                #check if folder already exists
                if [[ -f "$folder.zip" ]] 
                then
                    unzip -P $password $folder.zip
                    rm $folder.zip
                    
                    #count how many files in the unzipped folder. last_photo as in the number of files inside
                    last_photo=$(find $folder -type f | wc -l)

                    #download
                    for(( i = 1+$last_photo; i<= $pics+$last_photo; i++))
                    do
                        wget -O "$folder/PIC_$i.jpg" https://loremflickr.com/320/240  
                    done
                    #zipping, password required to open files
                    zip -r -P $password $folder.zip $folder/
                    rm -rf $folder

                else
                    mkdir $folder
                    
                    #download
                    for(( i = 1; i<= $pics; i++))
                    do
                        wget -O "$folder/PIC_$i.jpg" https://loremflickr.com/320/240  
                    done
                    #zipping, password required to open files
                    zip -r -P $password $folder.zip $folder/
                    rm -rf $folder
                    
                fi


                
                ;;
            "att")

                awk -v user="${username}" '
                    BEGIN {count=0;}  
                    { if ($5 == user || $10 == user) count+=1} 
                    END {print "attempt = " count}
                ' log.txt

                ;;
        esac

    else
        echo "$datetime  LOGIN: ERROR Failed login attempt  on user $username"
        echo "$datetime  LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    fi



}

#call function
login
