#!/bin/bash

mkdir -p users
echo -n "" >> users/user.txt

registering()
{
    read -p "Enter username : " username
    read -s -p "Enter password : " password

    if [[ "$password" == "$username" ]]
    then 
        echo "Password and username cannot be the same"
        exit 0 
    fi

    if [ ${#password} -lt 8 ] 
    then 
        echo "Password should contain at least 8 characters" 
        exit 0 
    fi

    if [[ "$password" =~ [^a-zA-Z0-9] ]] 
    then 
        echo "Password must be alphanumerical " 
        exit 0 
    fi

    if ! [[ "$password" =~ [A-Z] && "$password" =~ [a-z] ]]
    then 
        echo "Password must  contain both lower and uppercase" 
        exit 0 
    fi


    datetime=$(date '+%m/%d/%y %H:%M:%S')

    if grep -q "$username" users/user.txt
    then
        echo "$datetime REGISTER: ERROR User already exists" >> log.txt

    else
        echo "$username, $password" >> users/user.txt
        echo "$datetime REGISTER: INFO User $username registered successfully"
        echo "$datetime REGISTER: INFO User $username registered successfully" >> log.txt
    fi
}

#call function
registering

