# Soal Shift Sistem Operasi Modul-1 ITA05
Anggota kelompok:
- Sarah Hanifah Pontoh - 5027201006
- Rama Muhammad Murshal - 5027201041
- Muhammad Rifqi Fernanda - 5027201050

# Daftar Isi
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)

## Soal 1
Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.
Secara singkat program yang diminta adalah untuk membuat sistem reguster, dimana setelah register user akan melakukan login dan melakukan beberapa command.

### Soal 1.a
Membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. 

### Pembahasan 1.a
Pertama-tama membuat direktori untuk menyimpan user.txt

```shell
mkdir -p users
echo -n "" >> users/user.txt

```
Lalu membuat input untuk username dan password

```shell
read -p "Enter username : " username
read -s -p "Enter password : " password

```
`-s` pada password diperlukan agar inputan user hidden

### Soal 1.b
Membuat kriteria password user sesuai ketentuan:
1. Minimal 8 karakter
2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
3. Alphanumeric
4. Tidak boleh sama dengan username

### Pembahasan 1.b
Menggunakan if else untuk masing-masing kriteria

```shell
if [[ "$password" == "$username" ]]
then 
	echo "Password and username cannot be the same"
	exit 0 
fi

if [ ${#password} -lt 8 ] 
then 
	echo "Password should contain at least 8 characters" 
	exit 0 
fi

if [[ "$password" =~ [^a-zA-Z0-9] ]] 
then 
	echo "Password must be alphanumerical " 
	exit 0 
fi

if ! [[ "$password" =~ [A-Z] && "$password" =~ [a-z] ]]
then 
	echo "Password must  contain both lower and uppercase" 
	exit 0 
fi

```
Percobaan register dengan tes password sesuai kriteria:

![command](./images/register.png)

### Soal 1.c
Setiap percobaan login dan register akan tercatat pada **log.txt** dengan format : MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
1. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
2. Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
3. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
4. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

### Pembahasan 1.c
Kondisi 1 dan 2 diletakkan pada **register.sh**. Pertama-tama mengecek terlebih dahulu apakah username sudah ada di **user.txt** `if grep -q "$username" users/user.txt`. Lalu mengeluarkan message yang dicatat pada **log.txt** sesuai kondisi.

```shell
if grep -q "$username" users/user.txt
then
	echo "$datetime REGISTER: ERROR User already exists" >> log.txt

else
	echo "$username, $password" >> users/user.txt
	echo "$datetime REGISTER: INFO User $username registered successfully"
	echo "$datetime REGISTER: INFO User $username registered successfully" >> log.txt
fi

```

Sementara itu, kondisi 3 dan 4 diletakkan pada **main.sh**. Pertama mengecek dulu pasangan username dan password apakah sesuai atau tidak `if grep -q "$username, $password" users/user.txt`. Lalu mengeluarkan message yang dicatat pada **log.txt** sesuai kondisi.

```shell
if grep -q "$username, $password" users/user.txt
then
	echo -e "$datetime LOGIN:INFO User $username logged in!\n"
	echo "$datetime LOGIN:INFO User $username logged in!" >> log.txt

	#getting command from user 
	.
	.
	.

```
```shell
	.
	.
	.
else
	echo "$datetime  LOGIN: ERROR Failed login attempt  on user $username"
	echo "$datetime  LOGIN: ERROR Failed login attempt on user $username" >> log.txt
fi

```


### Soal 1.d
Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :
1. dl N ( N = Jumlah gambar yang akan didownload)
	Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.
2. att
	Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

### Pembahasan 1.d
Kedua command dilakukan setelah user masuk ke percabangan **then** di **main.sh**. User lalu diminta untuk menginput command yang diinginkan `read -p "Enter command : " command`. 

Jika user mengetikkan command **"dl"**, maka pertama-tama user akan diminta untuk menginput jumlah gambar yang ingin di-download `read -p "how many pic(s) : " pics`. Lalu akan dicek apabila sudah terdapat folder zip dengan nama yang sama pada direktori.
```shell
#check if folder already exists
if [[ -f "$folder.zip" ]]  

```

Jika **belum** ada folder zip tersebut, maka akan dibuat terlebih dahulu sesuai ketentuan nama `folder=$(date +%Y-%m-%d)_$username`.
Lalu proses download akan berjalan dengan menggunakan **i** sebagai iterator. Setelah di-download, folder tersebut akan di-zip, dan folder yang awal akan di remove menggunakan `rm` agar tidak double. Password akan diminta ke user saat ingin membuka file zip.
```shell
else
	mkdir $folder
	
	#download
	for(( i = 1; i<= $pics; i++))
	do
		wget -O "$folder/PIC_$i.jpg" https://loremflickr.com/320/240  
	done
	#zipping, password required to open files
	zip -r -P $password $folder.zip $folder/
	rm -rf $folder
	
fi

```
Download dan zip untuk pertama kalinya:

![command](./images/dl1.png)


Jika folder zip tersebut **sudah** ada, maka proses download akan "dilanjutkan" di folder tersebut. Pertama folder zip harus di unzip terlebih dahulu, lalu akan dihitung berapa jumlah file yang ada. `last_photo` menunjukkan jumlah file terakhir dan akan di-increment untuk men-download sampai sebanyak jumlah gambar yang diminta user. Proses zip sama seperti sebelumnya.

```shell
then
	unzip -P $password $folder.zip
	rm $folder.zip
	
	#count how many files in the unzipped folder. last_photo as in the number of files inside
	last_photo=$(find $folder -type f | wc -l)

	#download
	for(( i = 1+$last_photo; i<= $pics+$last_photo; i++))
	do
		wget -O "$folder/PIC_$i.jpg" https://loremflickr.com/320/240  
	done
	#zipping, password required to open files
	zip -r -P $password $folder.zip $folder/
	rm -rf $folder

```
Download dan zip jika folder sudah pernah ada:

![command](./images/dl2.png)

Password akan diminta ketika user akan membuka file:

![command](./images/zip_pass.png)

Jika user mengetikkan command **"att"**, maka program akan menghitung jumlah attempt login user menggunakan **awk** dengan membaca frekuensi kemunculan username pada file **log.txt**. Untuk message LOGIN, username muncul pada kata ke-5 (jika login berhasil) atau ke-10 (jika login gagal) sehingga `count` akan bertambah setiap menemukan username user sekarang.

```shell
awk -v user="${username}" '
	BEGIN {count=0;}  
	{ if ($5 == user || $10 == user) count+=1} 
	END {print "attempt = " count}
' log.txt

```
Menghitung attempt login user:

![command](./images/att.png)

### Kendala Soal 1
Kendala yang dialami adalah sedikit bingung dengan command zip yang mengharuskan password. Awalnya kami mengira user harus menginputkan password terlebih dahulu, ternyata maksud soal adalah folder zip tersebut diberi password sebelum user dapat membuka filenya, `"folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut"`.

## Soal 2
Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:

### Soal 2.a
Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.

### Pembahasan 2.a
Pada awal script `soal2_forensic_dapos.sh`, kita melakukan code `mkdir -p` untuk membuat folder forensic_log_website_daffainfo_log sesuai:

```shell
mkdir -p forensic_log_website_daffainfo_log

```

### Soal 2.b
Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.

### Pembahasan Soal 2.b
Untuk mengerjakan soal 2.b, kita hanya perlu menghitung total baris yang ada pada file log (kecuali pada baris pertamanya, pengecualian dengan `NR!=1`), lalu bagi dengan 12 (jumlah jam penyerangan). Kode seperti berikut:

```shell
awk -F : '
  {
    if(NR!=1) {
      ++n
    }
  } END {
    print "Rata-rata serangan adalah sebanyak " (n)/12 " requests per jam"
  }
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt
```

Hasil pada file ratarata.txt:

![image](./images/hasil_rata_rata.png)

### Soal 2.c
Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.

### Pembahasan soal 2.c
Untuk mengerjakan no 2c, kita terlebih dahulu print element pertama dari setiap baris (dipisahkan dengan ":" menggunakan `awk -F :`) (tidak termasuk baris pertama), lalu keluarannya akan di sort, lalu hasil sortnya akan dihilangkan yang sejenis dan dihitung berapa banyak menggunakan `uniq -c`, lalu akan kembali di sort menggunakan `sort -nr` untuk sort numerical secara descending, lalu print argument $1 dan $2 nya yang ada pada baris pertama (menggunakan `head -n 1`) sesuai format yang ditetapkan. Kode sesuai dengan:

```shell
awk -F : '
  {
    if(NR!=1) {
      print $1
    }
  }
' log_website_daffainfo.log | sort | uniq -c | sort -nr | head -n 1 | awk '
  {
    print "IP yang paling banyak mengakses server adalah: " $2 " sebanyak " $1 " requests \n"
  }
' >> forensic_log_website_daffainfo_log/result.txt
```

### Soal 2.d
Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl? Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.

### Pembahasan Soal 2.d
Untuk mengerjakan 2.d, kita hanya perlu mencari baris dari file log yang mengandung karakter `curl`. Jumlah barisnya tersebut kemudian kita print sesuai format yang ditentukan. Kode sesuai dengan:

```shell
awk -F : '
/curl/ { ++n }
END {
  print "Ada " n " request yang menggunakan curl sebagai user-agent \n"
}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```

### Soal 2.e
Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.

### Pembahasan soal 2.e
Untuk mengerjakan 2.e, kita hanya perlu melakukan perkondisian, apabila argument ketiga pada setiap barisnya (yang dipisahkan dengan : menggunakan awk) (tidak untuk baris pertama) sama denga 02, maka print argumen pertamanya. Kode sesuai dengan:

```shell
awk -F : '
$3 == 02 { print $1 " jam 2 pagi" }
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```

Hasil pada file result.txt

![image](./images/hasil_result.png)

### Kendala soal no 2
Adapun kendala yang kelompok kami dapatkan dalam mengerjakan soal nomor 2 adalah pada awal pengerjaannya belum sempat mengetahui bahwa dalam shell terdapat beberapa fungsi seperti `sort`, `uniq`, dan `head` yang dapat memudahkan pengerjaan / penyelesaian dengan algoritma.

## Soal 3
Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

### Note Soal 3 (in earlier)
- nama file untuk script per menit adalah minute_log.sh
- nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh
- semua file log terletak di /home/{user}/log


### Soal 3.a
Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.

### Pembahasan 3.a
Pertama-tama, mari kita lihat terlebih dahulu output dari perintah `free -m` saat dijalankan di terminal:

![command](./images/free-m.png)

Sedangkan untuk perintah `du -sh` menghasilkan keluaran: 

![command](./images/output_du-sh_command.png)

Berdasarkan keluaran tersebut, untuk mendapatkan data-data yang ada kemudian diolah menjadi format tertentu (berdasarkan gambar di bawah), kita dapat menggunakan konsep **AWK** yang akan memproses suatu input secara baris-perbaris.

```
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,sw ap_free,path,path_size
15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M
```

Pertama-tama, karena script shell yang akan dibuat akan menghasilkan suatu file .log yang memiliki nama sesuai: metrics_{YmdHms}.log dan akan disimpan pada /home/{user}/log, maka di awal file minute_log.sh kita buat terlebih dahulu sebuah variable yang akan menyimpan nilai path tersebut:

```shell
#!/bin/bash

mkdir -p /home/ramammurshal/log
date_time=$(date +"%Y%m%d%H%M%S")
output_path="/home/ramammurshal/log/metrics_$date_time.log"
```

Perintah `mkdir -p` untuk membuat folder /log pada /home/ramammurshal meskipun folder paretnnya belum dibuat dan`$(date +"%Y%m%d%H%M%S")` untuk menghasilkan string berdasarkan Y(tahun)m(bulan)d(tanggal)H(jam)M(menit)S(detik) saat file dibuat.

Setelah itu, kita dapat langsung melakukan print terhadap string mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,sw ap_free,path,path_size menggunakan perintah echo dan menyimpannya pada `$output_path` yang telah kita buat diatas menggunakan `>>` (menambahkan karakter meskipun isi dari file sudah ada).

```shell
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> $output_path
```

Setelah itu, kita coba untuk mendapatkan data memori menggunakan perintah `free -m` yang outputnya akan menjadi masukan dari perintah `awk` dengan menetapkan pola `/Mem:/`. Setiap kata pada keluaran akan diprint berdasarkan input argument dari `$2` hingga `$7`. Keluaran akan disimpan pada variable `memory`. Perintah tersebut berdasarkan:

```shell
memory="$(free -m | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s", $2,$3,$4,$5,$6,$7}')"
```

Untuk data swap, digunakan pola `/Swap:/` pada awk kemudian setiap kata keluarannya juga diprint berdasarkan input argumen `$2` hingga `$4`. Keluarannya tersebut akan disimpan pada variable `swap`. Perintah tersebut sesuai:

```shell
swap="$(free -m | awk '/Swap:/ {printf "%s,%s,%s", $2,$3,$4}')"
```

Untuk data storage, digunakan perintah `du -sh` dengan men-spesifiikan path sesuai `/home/ramammurshal/` yang outputnya akan menjadi masukan dari perintah `awk`, kemudian print berdasarkan argumen `$2` dan `$1`-nya. Keluaran tersebut disimpan pada variable `storage`. Perintah tersebut sesuai:

```shell
storage="$(du -sh /home/ramammurshal/ | awk '{printf "%s,%s",$2,$1}')"
```

Setelah mendapatkan data `memory`, `swap`, `storage`, selanjutnya kita print semua data yang tersimpan pada ketiga variable tersebut lalu menyimpannya pada `$output_path` sesuai:

```shell
echo "$memory,$swap,$storage" >> $output_path
```

Pada akhir script, kita melakukan pengubahan akses file sehingga hanya user pemilik file yang dapat membacanya menggunakan perintah:

```shell
chmod 700 $output_file
```

## Soal 3.b
Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.

## Pembahasan 3.b
Karena script minute_log.sh yang diminta harus berjalan otomatis pada tiap menit, sehingga kita dapat menggunakan `cron` untuk melakukan ini. Diketikkan `crontab -e` untuk membuat crontab baru, lalu pada bagian bawah script crontab, kita ketikkan `* * * * * bash /home/ramammurshal/minute_log.sh` yang berarti jalankan script shell tersebut tiap menitnya. Script tersebut kemudian akan menghasilkan file .log berdasarkan nama metrics_{YmdHms}.log yang berisi data memori dan storage /home/ramammurshal

Hasil saat file minute_log.sh telah dijalankan melalui cron:

![image](./images/hasil_ss_minute_log.png)

Hasil salah satu file metrics log yang dijalankan tiap menit:

![image](./images/hasil_ss_1_file_minute_log.png)

## Soal 3.c
Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log

## Pembahasan 3.c
Pertama-tama, kita buat terlebih dahulu script shell dengan nama aggregate_minutes_to_hourly_log.sh. Kemudian, karena file tersebut akan menghasilkan suatu file log dengan nama metrics_agg_{YmdH}.log, sehingga pada awal file shell tersebut kita coba untuk membuat sebuah variable yang akan menyimpan nama file log tersebut disertai dimana ia akan disimpan. Kode tersebut sesuai dengan:

```shell
#/bin/bash

date_time=$(date +"%Y%m%d%H")
output_path="/home/ramammurshal/log/metrics_agg_$date_time.log"
location_path="/home/ramammurshal/"
```

Setelah itu, karena script shell aggregate_minutes_to_hourly_log.sh nantinya akan berisi data dari file log yang dihasilkan tiap menit sebelumnya, sehingga kita perlu untuk mendapatkan data-data dari file tersebut. Untuk mendapatkannya, kita buat sebuah fungsi bernama `list_content_file()` sesuai dengan:

```shell
list_content_file() {
	for file in $(ls /home/ramammurshal/log/metrics_2022* | grep $date_time)
	do 
		cat $file | grep -v mem 
	done
}
```

Di dalam fungsi tersebut, kita coba untuk menjalankan perulangan berdasarkan perintah `ls /home/ramammurshal/log/metrics_2022*` untuk mendapatkan semua file dengan awalan metrics_2020* pada /home/ramammurshal/log lalu print file yang terdapat sesuai dengan `$date_time` atau jam saat itu. Di dalam perulangan tersebut, kita coba untuk membaca setiap satuan filenya, tetapi hanya print baris yang tidak terdapat kata mem, ini sesuai dengan `cat $file | grep -v mem `.

Setelah itu, untuk memproses setiap data yang sudah di print pada fungsi `list_content_file()`, kita coba untuk membuat fungsi `get_sorting()` yang akan melakukan print sesuai data minimum, maximum, atau average setiap data per-kategorinya. Secara garis besar, untuk memanggil fungsi tersebut kita harus mem-passing jenis data apa yang akan didapatkan serta akan di sort berdasarkan apa. Keluaran dari fungsi tersebut adalah sebuah angka yang nantinya dapat disimpan pada sebuah variable. 

Pada awal fungsi `get_sorting()` tersebut kita coba untuk melakukan perkondisian terlebih dahulu berdasarkan sesuai argumen pertamanya. Argumen pertama tersebut menentukan apa jenis data yang akan di fetch. Kode perkondisian sesuai dengan:

```shell
if [ $1 == "mem_total" ]
	then
		index=1
	elif [ $1 == "mem_used" ]
	then
		index=2
	elif [ $1 == "mem_free" ]
	then
		index=3
	elif [ $1 == "mem_shared" ]
	then
		index=4
	elif [ $1 == "mem_buff" ]
	then
		index=5
	elif [ $1 == "mem_available" ]
	then
		index=6
	elif [ $1 == "swap_total" ]
	then
		index=7
	elif [ $1 == "swap_used" ]
	then
		index=8
	elif [ $1 == "swap_free" ]
	then
		index=9
	elif [ $1 == "path_size" ]
	then
		index=11
	fi
```

Setelah itu, kita coba melakukan operasi `AWK` untuk memproses datanya. Tetapi, sebelum masuk ke AWK, kita pass terlebih dahulu 2 buah variable, `ind` sesuai dengan nilai `$index`, serta pilihan sort melalui variable `choose` sesuai dengan argumen kedua pemanggilan fungsi. Di dalam awk tersebut, pertama-tama kita melakukan perkondisian sesuai nilai `$ind`. Sebagai gambaran, `$1` adalah data mem_total, `$2` adalah data mem_used, `$3` adalah data mem_free, dan seterusnya. Diakhir perkondisian akan selalu di lakukan operasi penjumlahan variable `total` berdasarkan `$ind`, dan variable `count` berdasarkan banyaknya perulangan yang terjadi. Kode perkondisan dan operasi penambahan sesuai:

```shell
if(min == "") {
	min=max=$ind
}
if($ind > max) {
	max=$ind
}
if($ind < min) {
	min=$ind
}

total+=$ind;
count+=1
```

Setelah itu, kita sisa melakukan print berdasarkan variable `choose` yang di-pass ke dalam awk. Di-notes kembali bahwa variable `choose` untuk menentukan akan di-print berdasarkan data `$ind` (apakah data min/max/avg nya). Hal ini sesuai kode:

```shell
if(choose == "max") {
    print max
}
if(choose == "min") {
    print min
}
if(choose == "avg") {
	average=total/count
	printf "%.0f", average
}
```

Keseluruhan kode pada `AWK` dalam fungsi `get_sorting()` adalah sebagai berikut:

```shell
    list_content_file | awk -F , '
	{
		if(min == "") {
			min=max=$ind
		}
		if($ind > max) {
			max=$ind
		}
		if($ind < min) {
			min=$ind
		}
		
		total+=$ind;
		count+=1

	} END {
		if(choose == "max") {
			print max
		}
		if(choose == "min") {
			print min
		}
		if(choose == "avg") {
			average=total/count
			printf "%.0f", average
		}
	}' ind=$index choose=$2
```

Sehingga, untuk mendapatkan data-data yang sesuai (misal mem_total data min), kita sisa memanggil fungsi `get_sorting()` dan pass argument yang sesuai. Kode sama dengan:

```shell
# mem_total
mem_total_min=$(get_sorting mem_total min)
mem_total_max=$(get_sorting mem_total max)
mem_total_avg=$(get_sorting mem_total avg)

# mem_used
mem_used_min=$(get_sorting mem_used min)
mem_used_max=$(get_sorting mem_used max)
mem_used_avg=$(get_sorting mem_used avg)

# mem_free
mem_free_min=$(get_sorting mem_free min)
mem_free_max=$(get_sorting mem_free max)
mem_free_avg=$(get_sorting mem_free avg)

# mem_shared
mem_shared_min=$(get_sorting mem_shared min)
mem_shared_max=$(get_sorting mem_shared max)
mem_shared_avg=$(get_sorting mem_shared avg)

# mem_buff
mem_buff_min=$(get_sorting mem_buff min)
mem_buff_max=$(get_sorting mem_buff max)
mem_buff_avg=$(get_sorting mem_buff avg)

# mem_available
mem_available_min=$(get_sorting mem_available min)
mem_available_max=$(get_sorting mem_available max)
mem_available_avg=$(get_sorting mem_available avg)

# swap_total
swap_total_min=$(get_sorting swap_total min)
swap_total_max=$(get_sorting swap_total max)
swap_total_avg=$(get_sorting swap_total avg)

# swap_used
swap_used_min=$(get_sorting swap_used min)
swap_used_max=$(get_sorting swap_used max)
swap_used_avg=$(get_sorting swap_used avg)

# swap_free
swap_free_min=$(get_sorting swap_free min)
swap_free_max=$(get_sorting swap_free max)
swap_free_avg=$(get_sorting swap_free avg)

# path_size
path_size_min=$(get_sorting path_size min)
path_size_max=$(get_sorting path_size max)
path_size_avg=$(get_sorting path_size avg)
```

Setelah itu, print semua variable diatas lalu simpan pada `$output_path`, kode sesuai dengan:

```shell
# printing
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> $output_path
echo "minimum,$mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$location_path,$path_size_min" >> $output_path
echo "maximum,$mem_total_max,$mem_used_max,$mem_free_max,$mem_shared_max,$mem_buff_max,$mem_available_max,$swap_total_max,$swap_used_max,$swap_free_max,$location_path,$path_size_max" >> $output_path
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$location_path,$path_size_avg" >> $output_path
```

Lalu, diakhir script, kita kembali mengubah perizinan file log (hanya user yang dapat membaca) dengan menjalankan perintah `chmod 700 $output_path`

Lalu, untuk dapat menjalankan script aggregate_minutes_to_hourly.sh setiap jam, kita sisa set sebuah cronjob dengan perintah: `* * * * * bash /home/ramammurshal/aggregate_minutes_to_hourly.sh`.

Contoh isi file metrics_agg log:

![image](./images/hasil_ss_agg_log.png)

### Soal 3.d
Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

### Pembahasan 3.d
Untuk menjadikan semua file log hanya dapat dibaca oleh user, kita dapat menjalankan perintah `chmod 700 $output_path`. Perintah `chmod` untuk mengubah perizinan user, angka tujuh pertama untuk menjadi user dapat melakukan read, write, dan execute pada file, 0 kedua untuk menjadikan group user tidak memiliki akses terhadap file, 0 ketiga untuk menjadikan orang lain/other juga tidak memiliki akses terhadap file.

Contoh hasil perubahan akses file log:

![image](./images/bukti_rwx.png)

### Kendala Soal no 3
Adapun kendala yang kelompok kami hadapi dalam mengerjakan soal nomor 3 adalah sulitnya menemukan pola untuk mengambil data dari semua file log yang dihasilkan setiap menitnya menggunakan `awk`.

