#!/bin/bash

mkdir -p /home/ramammurshal/log
date_time=$(date +"%Y%m%d%H%M%S")
output_path="/home/ramammurshal/log/metrics_$date_time.log"

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> $output_path

memory="$(free -m | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s", $2,$3,$4,$5,$6,$7}')"
swap="$(free -m | awk '/Swap:/ {printf "%s,%s,%s", $2,$3,$4}')"
storage="$(du -sh /home/ramammurshal/ | awk '{printf "%s,%s",$2,$1}')"
echo "$memory,$swap,$storage" >> $output_path

chmod 700 $output_path

# Cronjob!
# * * * * * /home/ramammurshal/shell/minute_log.sh
